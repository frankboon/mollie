<?php
namespace Mollie;
/*
=======================================================================
    File:        class.mollie.php

    Created:     16-01-2005
    Authors:      Mollie B.V. / Frank Boon
    Version:     v2.4 14-09-2010 (cURL support)
    Thanks to:	  Laurens van Alphen (www.keenondots.com)

    More information? Go to www.mollie.nl
========================================================================
    Possible returns:
========================================================================
    10 - succesfully sent
    20 - no 'username' given
    21 - no 'password' given
    22 - no or incorrect 'originator' given
    23 - no 'recipients' given
    24 - no 'message' given
    25 - no juiste 'recipients' given
    26 - no juiste 'originator' given
    27 - no juiste 'message' given
    29 - wrong parameter(s)
    30 - incorrect 'username' or 'password'
    31 - not enough credits
    98 - gateway unreachable
    99 - unknown error
========================================================================
    Possible returns (Premium SMS):
========================================================================
    40 - invalid shortcode
    41 - invalid mid (MO Message ID)
    42 - mid not found
    43 - invalid tariff
    44 - invalid member (of subscription)
    45 - combination of username and keyword/shortcode not found
    46 - member of subscription reached maximum number of messages
========================================================================
*/

class Mollie
{
    protected $username          = null;
    protected $password          = null;
    protected $md5_password      = false;

    protected $gateway           = 1;
    protected $originator        = null;
    protected $recipients        = array();
    protected $reference         = null;
    protected $type              = 'normal';

    protected $premium_shortcode = null;
    protected $premium_keyword   = null;
    protected $premium_tariff    = null;
    protected $premium_mid       = null;
    protected $premium_member    = null;

    protected $success           = false;
    protected $successcount	     = 0;
    protected $resultcode        = null;
    protected $resultmessage     = null;

    public function setGateway ($gateway) {
        $this->gateway = $gateway;
    }

    public function useMd5Password()
    {
        $this->md5_password = true;
    }

    public function setType ($type)
    {
        if (in_array($type, array('normal', 'long'))) {
            $this->type = $type;
        }
    }

    public function setLogin ($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function setOriginator ($originator)
    {
        $this->originator = $originator;
    }

    public function removeOriginator()
    {
        $this->originator = null;
    }

    public function addRecipients ($recipient)
    {
        array_push($this->recipients, $recipient);
    }

    /**
     * Remove a recipient from the recipient array.
     * If the param is null remove all recipients
     *
     * @param string | null $recipient
     */
    public function removeRecipient ($recipient = null)
    {
        if ($recipient !== null) {
            $this->recipients[$recipient] = null;
        } else {
            $this->removeAllRecipients();
        }
    }

    /**
     * Remove all recipients from the array
     */
    public function removeAllRecipients()
    {
        $this->recipients = array();
    }

    public function setReference ($reference)
    {
        $this->reference = $reference;
    }

    public function setPremium ($shortcode, $keyword, $tariff, $mid = null, $member = false)
    {
        $this->premium_shortcode = $shortcode;
        $this->premium_keyword   = $keyword;
        $this->premium_tariff    = sprintf("%03s",$tariff);
        $this->premium_mid       = $mid;
        $this->premium_member    = ($member === true) ? 'true' : 'false';
    }

    public function getSuccess()
    {
        return $this->success;
    }

    public function getSuccessCount()
    {
        return $this->successcount;
    }

    public function getResultCode()
    {
        return $this->resultcode;
    }

    public function resetResult()
    {
        $this->resultcode = null;
        $this->resultmessage = null;
        $this->success = false;
    }

    public function getResultMessage()
    {
        return $this->resultmessage;
    }

    public function sendSMS ($message)
    {
        $recipients = implode(',', $this->recipients);

        $xml = $this->_sendToHost('www.mollie.nl',
                                    '/xml/sms/',
                                    'gateway=' . urlencode($this->gateway) .
                                    '&username=' . urlencode($this->username) .
                                    (($this->md5_password) ? '&md5_password=' : '&password=') . urlencode($this->password) .
                                    '&originator=' . urlencode($this->originator) .
                                    '&recipients=' . urlencode($recipients) .
                                    '&type=' . urlencode($this->type) .
                                    '&message=' . urlencode($message) .
                                    (($this->reference !== null) ? '&reference=' . urlencode($this->reference) : '') .
                                    (($this->premium_shortcode !== null) ? '&shortcode=' . urlencode($this->premium_shortcode) : '') .
                                    (($this->premium_keyword !== null) ? '&keyword=' . urlencode($this->premium_keyword) : '') .
                                    (($this->premium_tariff !== null) ? '&tariff=' . urlencode($this->premium_tariff) : '') .
                                    (($this->premium_mid !== null) ? '&mid=' . urlencode($this->premium_mid) : '') .
                                    (($this->premium_member !== null) ? '&member=' . urlencode($this->premium_member) : ''));

        $this->recipients = array();

        $this->XMLtoResult($xml);
    }

    /**
     * Send a V-Card, all fields are optional
     *
     * @param string $name
     * @param string $surname
     * @param string $tel_main
     * @param string $tel_cell
     * @param string $tel_home
     * @param string $tel_work
     * @param string $fax
     * @param string $email
     * @param string $url
     * @param string $address
     * @param string $note
     */
    public function sendVCard ($name = null, $surname = null, $organisation = null, $tel_main = null, $tel_cell = null,
                               $tel_home = null, $tel_work = null, $fax = null, $email = null,
                               $url = null, $address = null, $note = null)
    {
        $recipients = implode(',', $this->recipients);

        $xml = $this->_sendToHost('www.mollie.nl',
                                    '/xml/sms/',
                                    'gateway=' . urlencode($this->gateway) .
                                    '&username=' . urlencode($this->username) .
                                    (($this->md5_password) ? '&md5_password=' : '&password=') . urlencode($this->password) .
                                    '&originator=' . urlencode($this->originator) .
                                    '&recipients=' . urlencode($recipients) .
                                    '&type=vcard' .
                                    (($name !== null) ? '&vcard[name]=' . urlencode($name) : '') .
                                    (($surname !== null) ? '&vcard[surname]=' . urlencode($surname) : '') .
                                    (($organisation !== null) ? '&vcard[organisation]=' . urlencode($organisation) : '') .
                                    (($tel_main !== null) ? '&vcard[tel_main]=' . urlencode($tel_main) : '') .
                                    (($tel_cell !== null) ? '&vcard[tel_cell]=' . urlencode($tel_cell) : '') .
                                    (($tel_home !== null) ? '&vcard[tel_home]=' . urlencode($tel_home) : '') .
                                    (($tel_work !== null) ? '&vcard[tel_work]=' . urlencode($tel_work) : '') .
                                    (($fax !== null) ? '&vcard[tel_fax]=' . urlencode($fax) : '') .
                                    (($email !== null) ? '&vcard[email]=' . urlencode($email) : '') .
                                    (($url !== null) ? '&vcard[url]=' . urlencode($url) : '') .
                                    (($address !== null) ? '&vcard[address]=' . urlencode($address) : '') .
                                    (($note !== null) ? '&vcard[note]=' . urlencode($note) : '') .
                                    (($this->reference !== null) ? '&reference=' . urlencode($this->reference) : '') .
                                    (($this->premium_shortcode !== null) ? '&shortcode=' . urlencode($this->premium_shortcode) : '') .
                                    (($this->premium_keyword !== null) ? '&keyword=' . urlencode($this->premium_keyword) : '') .
                                    (($this->premium_tariff !== null) ? '&tariff=' . urlencode($this->premium_tariff) : '') .
                                    (($this->premium_mid !== null) ? '&mid=' . urlencode($this->premium_mid) : '') .
                                    (($this->premium_member !== null) ? '&member=' . urlencode($this->premium_member) : ''));

        $this->recipients = array();

        $this->XMLtoResult($xml);
    }

    public function sendMMS ($subject, $message, $attachment = null)
    {
        $recipients = implode(',', $this->recipients);

        $xml = $this->_sendToHost('www.mollie.nl',
                                    '/xml/mms/',
                                    'gateway=' . urlencode($this->gateway) .
                                    '&username=' . urlencode($this->username) .
                                    (($this->md5_password) ? '&md5_password=' : '&password=') . urlencode($this->password) .
                                    '&originator=' . urlencode($this->originator) .
                                    '&recipients=' . urlencode($recipients) .
                                    '&subject=' . urlencode($subject) .
                                    '&message=' . urlencode($message) .
                                    (($attachment !== null)	? '&attachment[1]=' . urlencode($attachment) : '') .
                                    (($this->reference !== null) ? '&reference=' . urlencode($this->reference) : ''));

        $this->recipients = array();

        $this->XMLtoResult($xml);
    }

    protected function _sendToHost($host, $path, $data)
    {
        if (function_exists('curl_init')) {
                return $this->_sendCurlToHost($host, $path, $data);
        }
        else {
                return $this->_sendFsockToHost($host, $path, $data);
        }
    }

    protected function _sendFsockToHost ($host, $path, $data)
    {
        $fp  = @fsockopen($host,80);
        $buf = '';
        if ($fp)
        {
            @fputs($fp, "POST $path HTTP/1.0\n");
            @fputs($fp, "Host: $host\n");
            @fputs($fp, "Content-type: application/x-www-form-urlencoded\n");
            @fputs($fp, "Content-length: " . strlen($data) . "\n");
            @fputs($fp, "Connection: close\n\n");
            @fputs($fp, $data);
            while (!feof($fp)) {
                    $buf .= fgets($fp,128);
            }
            fclose($fp);
        }

        list($headers, $body) = preg_split("/(\r?\n){2}/", $buf, 2);

        return $body;

    }

    protected function _sendCurlToHost ($host, $path, $data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://' . $host . $path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $body = curl_exec($ch);

        curl_close($ch);

        return $body;
    }

    protected function XMLtoResult ($xml)
    {
        $data = simplexml_load_string($xml);

        $this->success       = ($data->item->success == 'true');
        $this->successcount	 = $data->item->recipients;
        $this->resultcode    = $data->item->resultcode;
        $this->resultmessage = $data->item->resultmessage;
    }
}

